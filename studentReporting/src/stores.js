import Vue from 'vue'
import Vuex from 'vuex'
import { getToken } from './services'
import createLogger from 'vuex/dist/logger'
import {actions} from './actions/actionTodos'
Vue.use(Vuex)
const state = {
  count: '',
  token: '',
  user: ''
}

export default new Vuex.Store({
  state,
  plugins: [process.env.NODE_ENV !== 'production' ? createLogger() : ''],
  mutations: {
    SUBMIT_TEST_CODE: (state, data) => {
      state.data = data
    },
    getTokenState: (state) => {
      getToken().then(res => {
        state.token = res.data.data.token
      })
    }
  },
  actions,
  getters: {
    token: state => {
      return state.token
    }
  }
})
