import {getTestAndClassForAssignment} from '../services'

export const actions = {
  'SUBMIT_TEST_CODE': (context, payload) => {
    getTestAndClassForAssignment(payload.textCode).then(data => {
      context.commit('SUBMIT_TEST_CODE', data)
    }).catch(err => console.log(err))
  }
}
