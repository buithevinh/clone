
import axios from 'axios'
import store from '../stores'
export const interceptors = () => {
  axios.interceptors.request.use(
    requestConfig => {
      var token = store.getters.token
      if (token) {
        requestConfig.headers.Authorization = `Bearer ${token}`
        console.log(requestConfig)
      }
      return requestConfig
    },
    (requestError) => {
      return Promise.reject(requestError)
    }
  )

  // Add a response interceptor
  axios.interceptors.response.use(
    response => response,
    (error) => {
      return Promise.reject(error)
    }
  )
}

export function getToken () {
  return new Promise((resolve, reject) => {
    axios.get(
      'http://localhost:8081/static/data/token.json'
    ).then(data => {
      resolve(data)
    }).catch(err => reject(err))
  })
}

export function getTestAndClassForAssignment (testCode) {
  return new Promise((resolve, reject) => {
    const config = {
      param: {
        code: testCode,
        t: new Date().getTime(),
        sectionId: '',
        token: store.getters.token
      }
    }
    axios.get(
      'https://reqres.in/api/users?page=2', config)
      .then(response => {
        if (response.status === 200) {
          resolve(response.data)
        } else {
          reject(response.err)
        }
      })
  })
}
